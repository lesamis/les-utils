module.exports = {
	DestinationService: require('./lib/DestinationService'),
	ProviderService: require('./lib/ProviderService'),
	isAuthenticated: require('./lib/isAuthenticated'),
	parseQuery: require('./lib/parseQuery'),
	routes: require('./lib/routes')
}