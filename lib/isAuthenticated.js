const request 	= require('request');
const authURL 	= require('config').get('auth');

module.exports = function(req, res, next) {
  request({
    method: 'GET',
  	url: authURL.authenticate,
  	headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'jwt': req.headers.jwt || req.query.token
    }
  }, function(err, response, body) {
  	if(err) {
      return res.status(400).send(err);
    }

    if(response.statusCode === 401) {
      return res.status(401).json({error: 'You are not logged in'});
    }

    try {
      body = JSON.parse(body);
    } catch(e) {
      return res.status(400).send(e);
    }

    req.user = body;

    return next();
  });
};
