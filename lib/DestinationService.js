const request = require('request');
const _ = require('lodash');
const URL	= require('config').get('destinos');

const getCityWithProviders = async (cityId) => {
	if (!cityId) {
		return null;
	}

	const city = await fetch(`${URL}/api/cities/${cityId}/providers`);

	if (!city) {
		throw new Error('City not found');
	}

	const { Zones, Provider, ...rest } = city;
	
	const array = [{
		...rest,
		..._.omit(Provider, ['id', 'city_id', 'zone_id']),
	}].concat(
		Zones.map((zone) => {
			return {
				...rest,
				name: `${rest.name} - ${zone.name}`,
				..._.omit(zone.Provider, ['id', 'city_id', 'zone_id']),
			};
		}),
	);

	return array;
};

const fetch = (url, method = 'GET', qs = null) => new Promise((resolve, reject) => {
	const options = {
    method: method,
    keepAlive: false,
    url: url,
    headers: {
      'Accept-Encoding': 'gzip',
    },
		gzip: true,
		json: true,
	};
	
	if (qs) {
		options.qs = qs;
	}

  request(options, function (error, response, body) {
		if (error) {
			return reject(error);
		}

  	if (!body) {
			return resolve(null);
		}

    return resolve(body);
  });
});


const searchAirportsByCode = (codes) => {
	return fetch(`${URL}/api/airports/by-codes?codes=${codes}`);
}

const searchCityByCode = (code) => {
	return fetch(`${URL}/api/cities`, 'GET', {
		code,
		attributes: 'id,name,slug,country_id,instant,active',
		limit: 10,
		order: 'name ASC',	
	});
}

module.exports = {
	searchAirportsByCode,
	searchCityByCode,
	getCityWithProviders,
};