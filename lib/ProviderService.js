const request 	= require('request');
const URL				= require('config').get('payments');
//const URL				= 'http://190.210.188.249';
/**
*	Busca un destino
* Primero busca en el cache local, sino va a buscar a la app de destinos
*
*	@param id 			Int - id del destino
* @return					Promise | Callback
*/

const serialize = function(obj, prefix) {
  var str = [], p;
  for(p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}


const search = (params, cb) => {
	return new Promise((resolve, reject) => {
	  
	  if(!params) return resolve(null);

      params = serialize(params);

	  let options = {
	    method: 'GET',
	    keepAlive: false,
	    url: `${URL}/companies?${params}`,
	    headers: {
	      'Accept-Encoding': 'gzip',
	    },
	    gzip: true
	  };

	  let x = request(options, function (error, response, body) {
	  	if(typeof body == 'string') body = JSON.parse(body);

	  	if(!body) return resolve(null);
        console.log(body);
 	    return resolve(body);
	  });



	});
}


module.exports = {
	search: search
}